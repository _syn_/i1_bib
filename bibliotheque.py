class Book:
    def __init__(self, isbn, title, author, content,  genre, format):
        self.isbn = isbn
        self.title = title
        self.author = author
        self.content = content
        self.genre = genre
        self.format = format  # ebook or paper


class User:
    def __init__(self):
        self.books = []

    def add_book(self, book):
        self.books.append(book)
        print(f"Book added: {book.title}")

    def remove_book(self, isbn):
        for book in self.books:
            if book.isbn == isbn:
                self.books.remove(book)
                print(f"Book removed: {book.title}")
                return
        print("Book not found.")

    def modify_book(self, isbn, title, author, genre, format):
        for book in self.books:
            if book.isbn == isbn:
                book.title = title
                book.author = author
                book.genre = genre
                book.format = format
                print(f"Book data modified: {book.title}")
                return
        print("Book not found.")

    def display_books(self):
        if not self.books:
            print("The library is empty.")
        else:
            for book in self.books:
                print(f"ISBN: {book.isbn}, Title: {book.title}, Author: {book.author}, Content: {book.content}, Genre: {book.genre}, Format: {book.format}")


class Store:
    def __init__(self):
        self.books = []

    def add_book(self, book):
        self.books.append(book)
        print(f"Book added: {book.title}")

    def remove_book(self, isbn):
        for book in self.books:
            if book.isbn == isbn:
                self.books.remove(book)
                print(f"Book removed: {book.title}")
                return
        print("Book not found.")
        
    def modify_book(self, isbn, title, author, genre, format):
        for book in self.books:
            if book.isbn == isbn:
                book.title = title
                book.author = author
                book.genre = genre
                book.format = format
                print(f"Book data modified: {book.title}")
                return
        print("Book not found.")

    def display_books(self):
        if not self.books:
            print("The library is empty.")
        else:
            for book in self.books:
                print(f"ISBN: {book.isbn}, Title: {book.title}, Author: {book.author}, Content: {book.content}, Genre: {book.genre}, Format: {book.format}")


