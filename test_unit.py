import unittest
from bibliotheque import Book, User, Store

class TestLibraryFunctions(unittest.TestCase):
    def setUp(self):
        # Create sample books and users for testing
        self.book1 = Book(isbn="123456", title="Book 1", author="Author 1", content="Content 1", genre="Genre 1", format="ebook")
        self.book2 = Book(isbn="789012", title="Book 2", author="Author 2", content="Content 2", genre="Genre 2", format="paper")

        self.user = User()
        self.user.add_book(self.book1)
        self.user.add_book(self.book2)

        self.store = Store()
        self.store.add_book(self.book1)
        self.store.add_book(self.book2)

    def test_user_add_book(self):
        new_book = Book(isbn="345678", title="New Book", author="New Author", content="New Content", genre="New Genre", format="paper")
        self.user.add_book(new_book)
        self.assertIn(new_book, self.user.books)

    def test_user_remove_book(self):
        self.user.remove_book("123456")
        self.assertNotIn(self.book1, self.user.books)

    def test_user_modify_book(self):
        self.user.modify_book("123456", "Modified Title", "Modified Author", "Modified Genre", "paper")
        modified_book = [book for book in self.user.books if book.isbn == "123456"][0]
        self.assertEqual(modified_book.title, "Modified Title")
        self.assertEqual(modified_book.author, "Modified Author")
        self.assertEqual(modified_book.genre, "Modified Genre")
        self.assertEqual(modified_book.format, "paper")

    def test_user_display_books(self):
        # Redirect stdout to capture print output
        import sys
        from io import StringIO
        captured_output = StringIO()
        sys.stdout = captured_output

        self.user.display_books()

        # Reset redirect.
        sys.stdout = sys.__stdout__

        expected_output = "ISBN: 123456, Title: Book 1, Author: Author 1, Content: Content 1, Genre: Genre 1, Format: ebook\n" \
                          "ISBN: 789012, Title: Book 2, Author: Author 2, Content: Content 2, Genre: Genre 2, Format: paper\n"
        self.assertEqual(captured_output.getvalue(), expected_output)

    def test_store_add_book(self):
        new_book = Book(isbn="567890", title="New Book", author="New Author", content="New Content", genre="New Genre", format="ebook")
        self.store.add_book(new_book)
        self.assertIn(new_book, self.store.books)

    def test_store_remove_book(self):
        self.store.remove_book("789012")
        self.assertNotIn(self.book2, self.store.books)

    def test_store_modify_book(self):
        self.store.modify_book("789012", "Modified Title", "Modified Author", "Modified Genre", "ebook")
        modified_book = [book for book in self.store.books if book.isbn == "789012"][0]
        self.assertEqual(modified_book.title, "Modified Title")
        self.assertEqual(modified_book.author, "Modified Author")
        self.assertEqual(modified_book.genre, "Modified Genre")
        self.assertEqual(modified_book.format, "ebook")

    def test_store_display_books(self):
        # Redirect stdout to capture print output
        import sys
        from io import StringIO
        captured_output = StringIO()
        sys.stdout = captured_output

        self.store.display_books()

        # Reset redirect.
        sys.stdout = sys.__stdout__

        expected_output = "ISBN: 123456, Title: Book 1, Author: Author 1, Content: Content 1, Genre: Genre 1, Format: ebook\n" \
                          "ISBN: 789012, Title: Book 2, Author: Author 2, Content: Content 2, Genre: Genre 2, Format: paper\n"
        self.assertEqual(captured_output.getvalue(), expected_output)




if __name__ == '__main__':
    unittest.main()
