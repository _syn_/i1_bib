from abc import ABC, abstractmethod, abstractclassmethod

class Storage(ABC):
    @abstractmethod
    def save(self):
        pass

    @abstractclassmethod
    def readAll():
        pass

class User:
    def __init__(self, id, email, password):
        self.__id = id
        self.__email = email
        self.__password = password
        self.__books = set()

    @property
    def id(self):
        return self.__id
    
    @property
    def email(self):
        return self.__email
    
    @email.setter
    def email(self, value):
        self.__email = value

    @property
    def password(self):
        return self.__password
    
    @password.setter
    def password(self, value):
        self.__password = value


class Book:
    def __init__(self, id, author, title, content, available_in_store, store_price):
        self.__id = id
        self.__author = author
        self.__title = title
        self.__content = content
        self.__available_in_store = available_in_store
        self.__store_price = store_price

    @property
    def author(self):
        if not self.__sync_with_db:
            print("get author  from db")
            self.__sync_with_db = True

class BibApp:
    def __init__(self, id, email, password):
        pass